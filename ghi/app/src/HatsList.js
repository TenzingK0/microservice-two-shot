import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";


function HatColumn(props) {
  return (
    <div className="col">
      {props.hats.map(data => {
        const hat = data.hat;
        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img src={hat.picture} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                fabric: {hat.fabric}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                color: {hat.color}
              </h6>
              <h6 className="catd-subtitle mb-2 text-muted">
                Closet name: {hat.location.closet_name}
              </h6>
              <h6 className="catd-subtitle mb-2 text-muted">
                Section: {hat.location.section_number}
              </h6>
              <h6 className="catd-subtitle mb-2 text-muted">
                Shelf: {hat.location.shelf_number}
              </h6>
            </div>
            <div className="card-footer">
              <button onClick={ async () => {
                const hatUrl = `http://localhost:8090${hat.href}`
                const fetchConfig = {
                  method: "delete",
                }
                const response = await fetch (hatUrl, fetchConfig);
                if (response.ok) {
                  window.location.reload();
                }
              }} className="btn btn-danger">
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

function HatList(props) {
  const [HatColumns, setHatColumns] = useState([[], [], []]);

  const fetchData = async() => {
    const url = ' http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        setHatColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <h1 className="display-5 fw-bold">Wardrobify!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            This is where you can look at all of your hats! You can also create a new hat or delete on of your old hats!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a hat</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Hat Collection</h2>
        <div className="row">
          {HatColumns.map((hatList, index) => {
            return (
              <HatColumn key={index} hats={hatList} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default HatList;
