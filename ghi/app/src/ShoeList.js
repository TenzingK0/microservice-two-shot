import React, { useState, useEffect } from 'react';
import { useNavigate, NavLink, Link } from 'react-router-dom';



function ShoeList(props) {
  const [shoes, setShoes] = useState(props.shoes);
  const navigate = useNavigate();


  const handleDelete = async (shoe) => {
    try {
      const response = await fetch(`http://localhost:8080${shoe.href}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      });
      if (response.ok) {
        setShoes(shoes.filter(s => s.href !== shoe.href));
        navigate('/shoes');
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    setShoes(props.shoes);
  }, [props.shoes]);


  return (
  <>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
      <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Create a shoe</Link>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture URL</th>
          <th>Bin</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      {shoes && shoes.map(shoe => {
          const id = shoe.id;
          return (
            <tr key={shoe.id}>
              <td>{shoe.manufacturer}</td>
              <td>
                <NavLink to={`/shoe/detail/${id}`}>
                  {shoe.model_name}
                </NavLink>
              </td>
              <td>{ shoe.color }</td>
              <td><img src={ shoe.picture_url } width="100" height="100"/> </td>
              <td>{ shoe.bin.bin_number }</td>
              <td>
                <button onClick={() => handleDelete(shoe)} type="button" className="btn btn-danger">Delete</button>
            </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}


export default ShoeList;
