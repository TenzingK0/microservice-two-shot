import React, {useEffect, useState} from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');


    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;


        const ShoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(ShoeUrl, fetchConfig);
        if (response.ok) {

          setManufacturer('');
          setModelName('');
          setColor('');
          setPictureUrl('');
          setBin('');

        }
      }

    const fetchData = async () => {

    const BinUrl = 'http://localhost:8100/api/bins/';

    const response = await fetch(BinUrl);

    if (response.ok) {
      const data = await response.json();

      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


    return (
        <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create New Shoe Listing</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value = {manufacturer} placeholder="Manufacturer" required type="text" name="Manufacturer" id="Manufacturer" className="form-control"/>
                <label htmlFor="name">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelChange} value = {model_name} placeholder="ModelName" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="starts">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value = {color} placeholder="Color" required type="text" name="Color" id="Color" className="form-control"/>
                <label htmlFor="ends">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value = {picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="ends">PictureUrl</label>
              </div>
              <div className="form-group mb-3">
                <select onChange={handleBinChange} required name = "bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                      {bins.map(bin => {
                          return (
                          <option value={bin.href} key={bin.href}>
                              {bin.closet_name}
                          </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    );
}


export default ShoeForm;
