import React, { useState, useEffect } from 'react';
import { useParams, NavLink, useNavigate } from 'react-router-dom';

function ShoeDetail(props) {
  const { id } = useParams();
  const [shoe, setShoe] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}`);
      if (response.ok) {
        const data = await response.json();
        setShoe(data);
      } 
    }
    fetchData();
  }, [id]);

  if (!shoe) {
    return <div>Shoe Not Found!</div>;
  }






  const handlePictureUrlClick = (e) => {
    e.preventDefault();

  };

  const handleShoeListClick = () => {
    navigate('/shoes');
  };


  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="card" style={{width: '18rem'}}>
            <img src={shoe.picture_url} alt="Shoe" className="card-img-top" style={{ width: "100%", objectFit: "cover" }} />
            <div className="card-body">
              <h5 className="card-title">{shoe.manufacturer}</h5>
              <h6 className="card-subtitle mb-2 text-muted">{shoe.model_name}</h6>
              <p className="card-text"><strong>Color:</strong> {shoe.color}</p>
              <p className="card-text"><strong>Bin Number:</strong> {shoe.bin.bin_number}</p>
              <p className="card-text" ><a href={shoe.picture_url} style={{ color: "white" }}>Home</a></p>
              <p className="nav-item">
              <NavLink className="btn btn-primary" to="/shoes" onClick={handleShoeListClick}>Shoe List</NavLink>
                </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default ShoeDetail;
