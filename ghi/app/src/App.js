import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatForm from './HatForm';
import HatList from './HatsList';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
import Nav from './Nav';
import ShoeDetail from './ShoeDetail';

function App(props) {
  const [shoes, setShoes] = useState(props.shoes);

  useEffect(() => {
    setShoes(props.shoes);
  }, [props.shoes]);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes" element={<ShoeList shoes={shoes} />} />
          <Route path="/shoe/detail/:id" element={<ShoeDetail />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
