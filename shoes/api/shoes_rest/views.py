from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]

# Create your views here.
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {"bin": BinVODetailEncoder()}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {"bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
    )
    else:
        content = json.loads(request.body)
        # Get the Bin object and put it in the content dict
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )



#DON'T NEED PUT but can add again at close to end line 83, "PUT"
#View for SINGLE shoe(s)
@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    #PUT WHICH WE DONT NEED
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         if "bin" in content:
    #             bin = Bin.objects.get(id=content["bin"])
    #             content["bin"] = bin
    #     except Bin.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Invalid bin id"},
    #             status=400,
    #         )
    #     Shoe.objects.filter(id=pk).update(**content)
    #     shoe = Shoe.objects.get(id=pk)
    #     return JsonResponse(
    #         shoe,
    #         encoder=ShoeDetailEncoder,
    #         safe=False,
    #     )
