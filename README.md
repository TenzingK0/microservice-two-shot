# Wardrobify

Team:

* Person 1 - Which microservice? Kurt is doing shoes!!!
* Tenzing Khantse - Which microservice? -hats microservice!

## Design

wardrobe (microservice)
|
|-- location (class)
|
|-- Bin (class)
|
|-- hats (microservice) [polling from wardrobe]
|   |
|   |-- LocationVO (class) [used by hats microservice]
|   |
|   |-- Hats (class)
|
|-- shoes (microservice) [polling from wardrobe]
    |
    |-- BinVO (class) [used by shoes microservice]
    |
    |-- Shoe (class)


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

RESPONSE:
The BinVO model has 4 atrributes: import_href, closet_name, bin_number, bin_size
import_href is a reference for the Bin Class that RESTful can use as an assignment for that Bin and acts sort of like an "ID", which can be referenced.
The other three attributes are being pulled by the poller and mapped/passing them into our microservice, from the Bin Class in Wardrobe Microservice so we can use RESTful on them.

The Shoe class has 5 attributes: manufacturer, model_name, color, picture_url, and bin.
"bin" is a foreign key that relateds to the BinVO object so that it can pull those attributes from the class BinVO when using RESTFUL. The bin is a foreign key that ties BinVO and maps all that data.

The __str__ is used on BOTH models and will return the name of the BinVO/Shoe model, respectively.

From here, my integration is working due to my poller; this will periodically fetch the data from the microservice with URL http://wardrobe-api:8000/api/bins/, I also assign this so it communicates back and forth to that microservice from my Shoes Microservice.

The shoes poller is retrieving data from that endpoint and maps BinVO in React app using the update_or_create methdod. This allows us to render/see the current status of bins in the wardrobe and shoes and keeps the data clean and updated.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

My hat microservice has a hats model which is supposed to track the style name, color, a picture of the hat, and the location in the wardrobe where the hat exists. So when I was creating the model I made sure there was a style name which will have a charfield, a fabric which will have a charfield, a color which will have a charfield, a picture which will have a urlfield, and a location foreign key. Since the location model is in another microservice, I polled the location data from the wardrobe microservice, I only polled the location href, the location closet name, the location section number, and the location shelf number. Then I created a location value object in the hat microservice models. The location value object model will contain an import href which is the location href, the closet name which is the location closet name, the section number which is the location section number, and shelf number which is the location shelf number.
