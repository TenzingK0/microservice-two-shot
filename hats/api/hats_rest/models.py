from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


class Hat(models.Model):
    style_name = models.CharField(max_length=150)
    fabric = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture = models.URLField()

    location = models.ForeignKey(
        LocationVO,
        related_name="Hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
